import { XsdTypes } from "./src/definitions/XsdTypes";
import { TypeHandler } from "./src/handlers/TypeHandler";
import { ComplexTypeManager } from "./src/handlers/ComplexTypeManager";

var fs = require('fs')
var xml2js = require('xml2js');
 
var parser = new xml2js.Parser();
const typeHandler = new TypeHandler()
///home/nicholas/Documentos/Projetos/netbank-server/wsdl-na/NAServicesBar.wsdl
///home/nicholas/Documentos/Projetos/netbank-server/wsdl-eb/EBServicesBar.wsdl
///home/nicholas/Documentos/Projetos/netbank-server/wsdl-wf/WFServices.wsdl
fs.readFile('/home/nicholas/Documentos/Projetos/netbank-server/wsdl-wf/WFServices.wsdl', function(err: any, data: any) {
    parser.parseString(data, (err: any, result: any) => {
        const schema = result['wsdl:definitions']['wsdl:types'][0]["schema"][0]
        
        const complexTypeManager = new ComplexTypeManager(typeHandler)
        schema['complexType'].forEach((element: any) => {
            const complexType = complexTypeManager.generateComplexType(element)

            complexTypeManager.addComplexType(complexType)
        });

        complexTypeManager.getComplexTypes().forEach((complexType) => {
            fs.writeFile("/home/nicholas/Documentos/tipos/" + complexType.name + ".ts", complexTypeManager.generateTextObject(complexType), (err: any) => {
                if(err) {
                    console.log(err)
                }
            })
        })

        
    });
});

typeHandler.addWsdlType(XsdTypes.STRING, "string")
typeHandler.addWsdlType(XsdTypes.DECIMAL, "number")
typeHandler.addWsdlType(XsdTypes.INT, "number")
typeHandler.addWsdlType(XsdTypes.INTEGER, "number")
typeHandler.addWsdlType(XsdTypes.BOOLEAN, "boolean")
typeHandler.addWsdlType(XsdTypes.DATE, "date")
typeHandler.addWsdlType(XsdTypes.TIME, "date")
typeHandler.addWsdlType(XsdTypes.BASE64, "string")