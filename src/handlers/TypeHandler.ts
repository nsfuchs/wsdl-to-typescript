import { XsdTypes } from "../definitions/XsdTypes";
import { ComplexType } from "../definitions/ComplexType";

export class TypeHandler {
    private types = Array<WsdlType>()
    private dependencies = Array<Dependence>()
    private basicTypes = Array<string>()

    constructor(defaultBasicTypes: boolean = true) {
        if(defaultBasicTypes) {
            this.addBasicType("boolean")
            this.addBasicType("string")
            this.addBasicType("number")
            this.addBasicType("Date")
        }
    }

    addWsdlType = (wsdlType: string, tsType: string) => {
        this.types.push({
            wsdlType,
            tsType
        })
    }

    addBasicType = (basicType: string) => {
        this.basicTypes.push(basicType)
    }

    addDependence = (className: string, importExpression: string) => {
        this.dependencies.push({
            className,
            importExpression
        })
    }

    translateWsdlType = (wsdlType: string) => {
        const aux = this.types.find((type) => {
            return type.wsdlType === wsdlType
        })

        return aux? aux.tsType : "string"
    }

    translateTsType = (tsType: string) => {
        const aux = this.types.find((type) => {
            return type.tsType === tsType
        })

        return aux? aux.wsdlType : XsdTypes.STRING
    }

    handleDependencies = (className: string, complexTypes: Array<ComplexType>) => {
        const complexType = complexTypes.find((value) => {
            return value.name === className
        })
        if(!!complexType) {
            return this.generateImportString(`{ ${complexType.name} }`, `./${complexType.name}`)
        }
        const importAux = this.dependencies.find((dependence) => {
            return dependence.className = className
        })

        if(importAux) {
            return importAux.importExpression
        }

        throw `No import expression for the class '${className}' was found`
    }

    isBasicType = (className: string) => {
        return !!this.basicTypes.find((type) => {
            return className === type
        })
    }

    generateImportString = (classAs: string, classDirectory: string) => {
        return `import ${classAs} from '${classDirectory}'`
    }
}

interface WsdlType {
    wsdlType: string
    tsType: string
}

export interface Dependence {
    className: string,
    importExpression: string
}