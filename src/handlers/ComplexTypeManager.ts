import { ComplexType } from "../definitions/ComplexType";
import { TypeHandler } from "./TypeHandler";
import { Attribute } from "../definitions/Attributes";

export class ComplexTypeManager {
    private typeHandler: TypeHandler
    private complexTypes = Array<ComplexType>()

    constructor(typeHandler: TypeHandler) {
        this.typeHandler = typeHandler
    }

    getComplexTypes = () => {
        return this.complexTypes
    }

    generateComplexType = (element: any) => {
        const {attributes, extendedClassName, dependencies} = this.generateAttributes(element)
        const complexType: ComplexType = {
            name: element['$'].name,
            attributes,
            extendedClassName,
            dependencies
        }

        return complexType
    }
    addComplexType = (complexType: ComplexType) => {
        this.complexTypes.push(complexType)
    }
    private generateTypeInfo = (value: any): TypeInfo => {
        const tipoAux = value.type.split(":")
    
        const prefix = tipoAux[0]
        const type = tipoAux[1]
        let returnType: string
        let isArray: boolean
        let dependence: string | undefined
        if(prefix === "xsd") {
            returnType = this.typeHandler.translateWsdlType(type),
            isArray = value.maxOccurs? true : false
        } else {
            const selectedComplexType = this.complexTypes.find((complexType) => {
                return complexType.name === type
            })
            if(type.match(/ArrayOf/)) {
                returnType = selectedComplexType!.attributes[0].type,
                isArray = true
            } else {
                returnType = selectedComplexType? selectedComplexType.name : type,
                isArray = value.maxOccurs? true : false
            }
        }
        
        if(!this.typeHandler.isBasicType(returnType)) {
            dependence = this.typeHandler.handleDependencies(returnType, this.complexTypes)
        }
        return {
            type: returnType,
            isArray,
            dependence
        }
    }
    private generateAttributes = (element: any) => {
        const attributes = Array<Attribute>()
    
        let sequence: any
        let extendedClassName: string | undefined = undefined
        const dependencies: {[k: string]: string} = {}
        if(element.complexContent) {
            const extension = element.complexContent[0].extension[0]
            extendedClassName = extension['$']['base'].split(":")[1]
            const complexType = this.complexTypes.find((type) => {
                return type.name === extendedClassName
            })
    
            if(!complexType) {
                throw `Class not found: ${extendedClassName}` 
            }

            dependencies[extendedClassName!!] = this.typeHandler.generateImportString(`{ ${extendedClassName} }`, `./${extendedClassName}`)
            
            sequence = extension.sequence
        } else {
            sequence = element.sequence
        }
    
        sequence[0].element.forEach((aux: any) => {
            const value = aux['$']
            const {type, isArray, dependence} = this.generateTypeInfo(value)

            if(!!dependence) {
                dependencies[type] = dependence
            }

            attributes.push({
                name: value.name,
                nillable: value.nillable && value.nillable === "true"? true : false,
                type: type,
                use: value.use && value.use === "true"? true : false,
                isArray: isArray
            })
        })
    
        return { 
            attributes,
            extendedClassName,
            dependencies
        }
    }
    private attributeToText = (attribute: Attribute) => {
        let stringAttribute = "    " + attribute.name + (attribute.nillable? "?: " : ": ")
        stringAttribute += attribute.type + (attribute.isArray? "[]" : "") + "\n"
    
        return stringAttribute
    }

    generateTextObject = (complexType: ComplexType) => {
        let fileText = ""
        for(let dependence in complexType.dependencies) {
            fileText += complexType.dependencies[dependence] + "\n\n"
        }

        fileText += `export interface ${complexType.name}`
        
        if(complexType.extendedClassName) {
            fileText += " extends " + complexType.extendedClassName
        }
        fileText += " {\n"
        
        complexType.attributes.forEach((attribute) => {
            fileText += this.attributeToText(attribute)
        })
        fileText += "}"
        
        return fileText
    }
}

export interface TypeInfo {
    type: string,
    isArray: boolean,
    dependence?: string
}