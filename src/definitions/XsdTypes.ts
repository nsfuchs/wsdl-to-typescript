export enum XsdTypes {
    STRING = "string",
    DECIMAL = "decimal",
    INTEGER = "integer",
    INT = "int",
    BOOLEAN = "boolean",
    DATE = "date",
    TIME = "time",
    BASE64 = "base64Binary"
}