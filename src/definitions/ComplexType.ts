import { Attribute } from "./Attributes";

export interface ComplexType {
    name: string,
    attributes: Array<Attribute>
    dependencies: {[k: string]: string}
    extendedClassName?: string
}