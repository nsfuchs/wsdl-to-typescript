export interface Attribute {
    name: string,
    nillable: boolean,
    type: string,
    use: boolean,
    maxOccurs?: string,
    isArray?: boolean,
    dependence?: string
}